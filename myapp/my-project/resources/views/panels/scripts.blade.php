<script src="{{asset('assets/js/jquery.js')}}"></script>
<script src="{{asset('assets/js/plugins.min.js')}}"></script>
<script src="{{asset('assets/js/functions.js')}}"></script>

@yield('page-scripts')

</body>
</html>
