<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0">
    <meta name="description" content="sw">
    <meta name="keywords" content="sw">
    <meta name="author" content="sw">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">




    <title>{{ 'System' }}</title>

    @include('panels.styles')

</head>

<body class="stretched">

<!-- Document Wrapper
============================================= -->
<div id="wrapper" class="clearfix">


    <header id="header" class="full-header" data-mobile-sticky="true">
        <div id="header-wrap">
            <div class="container">
                <div class="header-row">

                    <!-- Logo
                    ============================================= -->
                    <div id="primary-menu-trigger">
                        <svg class="svg-trigger" viewBox="0 0 100 100"><path d="m 30,33 h 40 c 3.722839,0 7.5,3.126468 7.5,8.578427 0,5.451959 -2.727029,8.421573 -7.5,8.421573 h -20"></path><path d="m 30,50 h 40"></path><path d="m 70,67 h -40 c 0,0 -7.5,-0.802118 -7.5,-8.365747 0,-7.563629 7.5,-8.634253 7.5,-8.634253 h 20"></path></svg>
                    </div>
                    <div id="logo">
                        <a href="index.html" style="height: 60px"  class="standard-logo" data-dark-logo="{{asset('assets/images/logo-dark.png')}}"><img src="{{asset('assets/images/logo 2.png')}}" alt="Canvas Logo"></a>
                        <a href="index.html" style="height: 40px" class="retina-logo" data-dark-logo="{{asset('assets/images/logo-dark@2x.png')}}"><img src="{{asset('assets/images/logo 2.png')}}" alt="Canvas Logo"></a>
                    </div><!-- #logo end -->

                    <div class="header-misc">

                        <!-- Top Search
                        ============================================= -->


                        <!-- Top Cart
                        ============================================= -->

                        <div id="top-cart" class="header-misc-icon">
                            <a href="#" id="top-cart-trigger"><i class="icon-line-bag"></i><span class="top-cart-number">5</span></a>
                            <div class="top-cart-content">
                                <div class="top-cart-title">
                                    <h4>  <a href="#"><img src="images/shop/small/6.jpg" alt="Light Blue Denim Dress" /></a>Puanalmighty     0 B</h4>
                                </div>


                                <div class="top-cart-items">
                                    <div class="top-cart-item">

                                        <div class="top-cart-item-desc">
                                            <div class="top-cart-item-desc-title">
                                                <a href="#">Light Blue Denim Dress</a>

                                            </div>

                                        </div>
                                    </div>
                                </div>
                                <div class="top-cart-action">

                                    <a href="#" class="button button-3d button-small m-0">Logout</a>
                                </div>

                            </div><!-- #top-cart end -->
                        </div>
                        <div class="dropdown mx-3 me-lg-0">
                            <a href="#" class="btn btn-secondary btn-sm dropdown-toggle" data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="true"><i class="icon-user"></i></a>
                            <ul class="dropdown-menu dropdown-menu-end" aria-labelledby="dropdownMenu1">
                                <a class="dropdown-item text-start" href="#">Profile</a>
                                <a class="dropdown-item text-start" href="#">ตะกร้่าสินค้า</a>
                                <a class="dropdown-item text-start" href="#">แนะนำเพื่อน</a>
                                <div class="dropdown-divider"></div>
                                <a class="dropdown-item text-start" href="#">ออกจากระบบ <i class="icon-signout"></i></a>
                            </ul>
                        </div>

                    </div>



                    <!-- Primary Navigation
                    ============================================= -->

                        @include('panels.navbar')

                </div>
            </div>
        </div>
        <div class="header-wrap-clone"></div>
    </header><!-- #header end -->
            @yield('content')





{{--    <section id="slider" class="slider-element vh-100" style="background: url('images/slider/full/3.jpg') center center; background-size: cover;"></section>--}}

    <!-- Content
    ============================================= -->




    @include('panels.footer')




@include('panels.scripts')



